//
// video.js
// 
// Collect video view statistics and push them to the server-side
//
// Works for the first video on the page

function logdebug(outputstring) {
	if (debug_enabled) {
		console.log(outputstring);
	}
}

function getCookie(cname) {
	var name = cname + "=";
	var ca = document.cookie.split(';');
	var val = "NOCOOKIEFOUND";
	for (var i = 0; i < ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0) == ' ') {
			c = c.substring(1);
		}
		if (c.indexOf(name) == 0) {
			val = c.substring(name.length, c.length);
		}
	}
	return val;
}

function videoLoaded() {
}

function videoSendUpdate(evName) {
	if (debugenabled) {
		document.getElementById("demo").innerHTML = "foo" + evName
				+ video.played.length;
		logdebug(evName + "Video time index " + video.currentTime + "s");
		logdebug("video played TimeRanges length " + video.played.length);
		for (i = 0; i < video.played.length; i++) {
			logdebug("Start: " + video.played.start(i) + " End: "
					+ video.played.end(i));
		}
	}

	// Only send actual data
	if (video.played.length > 0) {
		obj = {
			"contact_id" : getCookie("contact_id"),
			"duration" : video.duration,
			"timerange_length" : video.played.length,
			"video_index" : videoindex,
			"userid" : userid
		};
		for (i = 0; i < video.played.length; i++) {
			obj["start" + i] = video.played.start(i);
			obj["end" + i] = video.played.end(i);
		}
		val = JSON.stringify(obj);
		var xmlhttp = new XMLHttpRequest();
		xmlhttp.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200) {
				// Typical action to be performed when the document is ready:
				if (debugenabled) {
					document.getElementById("demo").innerHTML = xmlhttp.responseText;
				}
			}
		};
		// Asynchronous only for iOS on pagehide
		var synchro = true;
		if (evName == "pagehide") {
			synchro = false;
		}

		xmlhttp.open("POST", "../php/src/new_data.php", synchro);
		xmlhttp.setRequestHeader("Content-type",
				"application/x-www-form-urlencoded");
		xmlhttp.send("x=" + val);
	}
}

function videoBeforeUnload(event) {
	videoSendUpdate("BeforeUnload");
}

function videoPlay() {
	logdebug("PLAY Video time index " + video.currentTime + "s");
}

function videoPlaying() {
	logdebug("PLAYING Video time index " + video.currentTime + "s");
}

function videoEndedPlaying(event) {
	logdebug("ENDED Video time index " + video.currentTime + "s");
}

function videoPausedPlaying(event) {
	videoSendUpdate("Paused");
}

// iPhone handling, Apple likes pagehide and doesn't like beforeunload
//
var isOnIOS = true;
var eventName = isOnIOS ? "pagehide" : "beforeunload";
var video;

var userid = "";

//if data-myid doesn't exist then give up, don't add listeners

	userid = "FARBLE129";
	var debugenabled = 1;
	var videoindex = 0;

	var played = {length:1,start}
	video.played.length=1;
	video.currentTime=83;
	video.played.start(0)=1;
	video.played.end(0)=2;

