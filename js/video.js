//
// video.js
// 
// Collect video view statistics and push them to the server-side
//
// Works for the first video on the page
// TODO Abort, don't send updates if the contact hasn't been cookied
// TODO have cookie as a global, don't look it up every time

function logdebug(outputstring) {
	if (debugenabled) {
		console.log(outputstring);
	}
}

function getCookie(cname) {
	var name = cname + "=";
	var ca = document.cookie.split(';');
	var val = "NOCOOKIEFOUND";
	for (var i = 0; i < ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0) == ' ') {
			c = c.substring(1);
		}
		if (c.indexOf(name) == 0) {
			val = c.substring(name.length, c.length);
		}
	}
	return val;
}

function videoLoaded() {
	var videoarr = document.getElementsByTagName("video");
	if (videoarr.length > 0) {
		video = videoarr[videoindex];
		// TODO shutoff processing if no video is found
		video.addEventListener("play", videoPlay);
		video.addEventListener("playing", videoPlaying);
		video.addEventListener("ended", videoEndedPlaying);
		video.addEventListener("pause", videoPausedPlaying);
		if (debugenabled) {
			document.getElementById("demo").innerHTML = eventName;
		}
	} else {
		console.log("OPEXtras: No viable video found on page.");
	}
}

function videoSendUpdate(evName) {
	if (debugenabled) {
		document.getElementById("demo").innerHTML = "foo" + evName
				+ video.played.length;
		logdebug(evName + "Video time index " + video.currentTime + "s");
		logdebug("video played TimeRanges length " + video.played.length);
		for (i = 0; i < video.played.length; i++) {
			logdebug("Start: " + video.played.start(i) + " End: "
					+ video.played.end(i));
		}
	}

	// Only send actual data
	if (video.played.length > 0) {
		obj = {
			"contact_id" : getCookie("contact_id"),
			"duration" : video.duration,
			"timerange_length" : video.played.length,
			"video_index" : videoindex,
			"userid" : userid
		};
		for (i = 0; i < video.played.length; i++) {
			obj["start" + i] = video.played.start(i);
			obj["end" + i] = video.played.end(i);
		}
		val = JSON.stringify(obj);
		var xmlhttp = new XMLHttpRequest();
		xmlhttp.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200) {
				// Typical action to be performed when the document is ready:
				if (debugenabled) {
					document.getElementById("demo").innerHTML = xmlhttp.responseText;
				}
			}
		};
		// Asynchronous only for iOS on pagehide
		var synchro = true;
		if (evName == "pagehide") {
			synchro = false;
		}

		xmlhttp
				.open("POST",
						rootpath+"/OPEVVS/php/src/new_data.php",
						synchro);
		xmlhttp.setRequestHeader("Content-type",
				"application/x-www-form-urlencoded");
		xmlhttp.send("x=" + val);
	}
}

function videoBeforeUnload(event) {
	videoSendUpdate("BeforeUnload");
}

function videoPlay() {
	logdebug("PLAY Video time index " + video.currentTime + "s");
}

function videoPlaying() {
	logdebug("PLAYING Video time index " + video.currentTime + "s");
}

function videoEndedPlaying(event) {
	logdebug("ENDED Video time index " + video.currentTime + "s");
}

function videoPausedPlaying(event) {
	videoSendUpdate("Paused");
}

// iPhone handling, Apple likes pagehide and doesn't like beforeunload
//
var isOnIOS = navigator.userAgent.match(/iPad/i)
		|| navigator.userAgent.match(/iPhone/i);
var eventName = isOnIOS ? "pagehide" : "beforeunload";
var video;

var userid = "";

elmt = document.getElementById("OPEXTRAS");
var rootpath="https://www.opextras.com/OPE";
if (elmt.hasAttribute("data-operootpath")) {
	rootpath = elmt.getAttribute("data-operootpath");
}
//if data-myid doesn't exist then give up, don't add listeners
if (elmt.hasAttribute("data-myid")) {
	userid = elmt.getAttribute("data-myid");
	var debugenabled = 0;
	if (elmt.hasAttribute("debugenabled")) {
		debugenabled = elmt.getAttribute("debugenabled");
	}
	var videoindex = 0;

	addEventListener("load", videoLoaded);
	addEventListener(eventName, videoBeforeUnload);

} else {
	console.log("OPEXTRAS aborting: could not find data-myid attribute.")
}
