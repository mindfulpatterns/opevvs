//
function updateUserInformation(id, val) {
	console.log("updateAppID " + id + " " + val);
	var email = document.getElementById("email").innerHTML;
	var hashid = document.getElementById("hashthingy").innerHTML;
	obj = {
		email : email,
		hashid : hashid,
		field : id,
		value : val
	};
	console.log(obj);
	val = JSON.stringify(obj);
	var xmlhttp = new XMLHttpRequest();
	xmlhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			// Typical action to be performed when the document is ready:
			document.getElementById("vvsspinner").style.display = "none";
		}
	};
	document.getElementById("vvsspinner").style.display = "block";

	xmlhttp.open("POST", "/" + rootpath + "/OPECore/php/src/setUserInformation.php", true);
	xmlhttp.setRequestHeader("Content-type",
			"application/x-www-form-urlencoded");
	xmlhttp.send(val);
}

function OPauthCheck() {
	var email = document.getElementById("email").innerHTML;
	var hashid = document.getElementById("hashthingy").innerHTML;
	obj = {
		email : email,
		hashid : hashid
	};
	val = JSON.stringify(obj);
	var xmlhttp = new XMLHttpRequest();
	xmlhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			var myObj = JSON.parse(this.responseText);
			document.getElementById("vvsspinner").style.display = "none";
			//document.getElementById("authcheck").innerHTML = myObj.ret;
			if (myObj.ret == true) {
				document.getElementById("verifybutton").style.backgroundColor = "green";
			} else {
				document.getElementById("verifybutton").style.backgroundColor = "red";
			}
		}
	};
	document.getElementById("vvsspinner").style.display = "block";
	xmlhttp.open("POST", "/" + rootpath + "/OPEVVS/php/src/authCheck.php", true);
	xmlhttp.setRequestHeader("Content-type",
			"application/x-www-form-urlencoded");
	xmlhttp.send("x=" + val);

}

function getUserInformation() {
	var email = document.getElementById("email").innerHTML;
	var hashid = document.getElementById("hashthingy").innerHTML;
	obj = {
		email : email,
		hashid : hashid
	};
	val = JSON.stringify(obj);
	var xmlhttp = new XMLHttpRequest();
	xmlhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			var myObj = JSON.parse(this.responseText);
			document.getElementById("vvsspinner").style.display = "none";
			document.getElementById("appid").value = myObj.appid;
			document.getElementById("apikey").value = myObj.apikey;
			document.getElementById("jsheader").innerHTML = "&lt;script id=\"OPEXTRAS\" data-myid=\""
					+ myObj.userid
					+ "\" src=\"https://app.opextras.com/OPE/OPEVVS/js/video.js\"&gt;&lt;/script&gt;";

		}
	};
	document.getElementById("vvsspinner").style.display = "block";
	xmlhttp.open("POST", "/" + rootpath + "/OPECore/php/src/getUserInformation.php", true);
	xmlhttp.setRequestHeader("Content-type",
			"application/x-www-form-urlencoded");
	xmlhttp.send(val);

}

function CopyToClipboard(containerid) {
	if (document.selection) {
		var range = document.body.createTextRange();
		range.moveToElementText(document.getElementById(containerid));
		range.select().createTextRange();
		document.execCommand("copy");
		document.getElementById("button1").value = "copied";
		document.getElementById("button1").style.backgroundColor = "green";

	} else if (window.getSelection) {
		var range = document.createRange();
		range.selectNode(document.getElementById(containerid));
		window.getSelection().addRange(range);
		document.execCommand("copy");
		document.getElementById("button1").value = "copied";
		document.getElementById("button1").style.backgroundColor = "green";

	}
	return false;
}

function mktags(id, val) {
	obj = {
		url : val
	};
	val = JSON.stringify(obj);
	var xmlhttp = new XMLHttpRequest();
	xmlhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			var myObj = JSON.parse(this.responseText);
			document.getElementById("vvsspinner").style.display = "none";
			document.getElementById("list_of_tags").innerHTML = "Resulting tags:<br>"+myObj.tags;
		}
	};
	document.getElementById("vvsspinner").style.display = "block";

	xmlhttp.open("POST", "/" + rootpath + "/OPEVVS/php/src/getTagListFromURL.php", true);
	xmlhttp.setRequestHeader("Content-type",
			"application/x-www-form-urlencoded");
	xmlhttp.send("x=" + val);
}


elmt = document.getElementById("opextras_vvs_uijs");
var rootpath="OPE";
if (elmt.hasAttribute("data-operootpath")) {
	rootpath = elmt.getAttribute("data-operootpath");
}
addEventListener("load", getUserInformation);