<?php
/**
 * URLtoTag -
 * PHP Version 7
 *
 * @category  VVS
 * @package   VVS
 * @author    Brad Dobson <brad@opextras.com>
 * @copyright 2018 Mindful Patterns, LLC - All Rights Reserved
 * @license   http://www.opextras.com/ Proprietary
 * @link      d
 */

/**
 * URLtoTag class
 *
 * @category VVS
 * @package  VVS
 * @author   Brad Dobson <brad@opextras.com>
 * @license  foo http://www.opextras.com
 * @link     d
 */
class URLtoTag
{

    // Trying to figure out how to use file_get_contents for json
    // Not working yet.
    protected $fd;

    /**
     * Function getVideoNameFromURL
     *
     * @param string $url URL
     *
     * @return string
     */
    public function getVideoNameFromURL($url)
    {
        return (basename(parse_url($url, PHP_URL_PATH)));
    }

    /**
     * Function buildTag
     *
     * @param string $videoName name of the video
     * @param string $start     start time of the video
     * @param string $end       end time of the video
     *
     * @return string
     */
    public function buildTag($videoName, $start, $end)
    {
        return ($videoName . "_" . $start . "_" . $end);
    }

    /**
     * Function getJSONTagListFromURL
     *
     * @param array $inputarr $_POST or uts array
     *
     * @return string
     */
    public function getJSONTagListFromURL($inputarr)
    {
        $obj = new stdClass();
        if (array_key_exists("x", $inputarr)) {
            $obj = json_decode($inputarr["x"], false);
        } else {
            return;
        }
        $videoName = $this->getVideoNameFromURL($obj->url);
        $value = "";
        for ($j = 0; $j < 10; $j ++) {
            $value = $value . "<br>" . 
                $this->buildTag($videoName, ($j * 10), (($j + 1) * 10));
        }

        $myObj = new stdClass();
        $myObj->tags = $value;

        $myJSON = json_encode($myObj);

        echo $myJSON;
        return $myJSON;
    }
}
?>