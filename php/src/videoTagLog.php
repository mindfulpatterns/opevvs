<?php
/**
 * A thing
 * PHP Version 7
 * 
 * @category X
 * @package  Y
 * @author   Brad Dobson <brad@opextras.com>
 * @license  z 
 * @link     d
 * 
 * TODO figure out if tags field should be one record per tag
 * TODO Need basic way to log buckets if there's no contact_id
 */
require_once 'autoload.php';

/**
 * Some doc
 * 
 * @category X
 * @package  Y
 * @author   Brad Dobson <brad@opextras.com>
 * @license  z 
 * @link     d
 */
class VideoTagLog
{
    protected $db;

    protected $conn;

    protected $psm;

    protected $numberOfLogEntries;

    protected $averageTimeWatched;

    protected $averageDropoffTime;

    protected $duration;

    /**
     * Some doc
     */
    public function __construct()
    {
        $this->numberOfLogEntries = 0;
        $this->averageTimeWatched = 0;
        $this->avgtime = 0;
        $this->db = new opeDatabase();
        $this->conn = $this->db->getConn();
        date_default_timezone_set('UTC');
    }

    /**
     * Some doc
     * 
     * @return boolean
     */
    public function retrieveEntries()
    {
        $userid = $_GET["userid"];
        $pagename = $_GET["pname"];
        $query = sprintf(
            "SELECT AVG(totalTimeWatched) AS avgtime FROM 
            VIDEOTAGLOG WHERE userid='%s' AND pagename='%s'",
            $userid,
            $pagename
        );
        //error_log($query);
        if ($result = $this->conn->query($query)) {
            $row = $result->fetch_array(MYSQLI_ASSOC);
            $this->averageTimeWatched = $row['avgtime'];
        }
        $query = sprintf(
            "SELECT HEX(top50PercentViewed),HEX(bottom50PercentViewed),
            totalTimeWatched,duration FROM VIDEOTAGLOG WHERE userid='%s' 
            AND pagename='%s'", 
            $userid, 
            $pagename
        );
        // error_log($query);
        $ret = 0;
        $this->averageDropoffTime = 0;

        if ($result = $this->conn->query($query)) {
        }
        if ($result->num_rows) {
            $ret = $result->num_rows;
            $this->averageDropoffTime = 0;
            while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
                $this->duration = $row["duration"];
                $max = 0;
                for ($percentage = 1; $percentage <= 100; $percentage ++) {
                    $top = 0;
                    $bottom = 0;
                    sscanf($row['HEX(top50PercentViewed)'], "%16x", $top);
                    sscanf($row['HEX(bottom50PercentViewed)'], "%16x", $bottom);
                    $bits = new vvsPercentBits($top, $bottom);
                    if ($bits->getBit($percentage)) {
                        $max = $percentage;
                    }
                }
                $this->averageDropoffTime += $max;
            }
            $this->averageDropoffTime = $this->averageDropoffTime / 
                                        $result->num_rows;
        }
        $result->data_seek(0);

        $this->numberOfLogEntries = $result->num_rows;
        for ($percentage = 1; $percentage <= 100; $percentage ++) {
            $counter = 0;
            while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
                $top = 0;
                $bottom = 0;
                sscanf($row['HEX(top50PercentViewed)'], "%16x", $top);
                sscanf($row['HEX(bottom50PercentViewed)'], "%16x", $bottom);
                $bits = new vvsPercentBits($top, $bottom);
                if ($bits->getBit($percentage)) {
                    $counter ++;
                }
            }
            printf("[%d, %d],\n", $percentage, $counter);
            $result->data_seek(0);
        }
        return $ret;
    }

    
    /**
     * Some doc
     * 
     * @return boolean
     */
    public function showStats()
    {
        printf("STATS");
        printf("<table>");
        printf(
            "<tr><td>Duration(h:m:s)<td>%s (%ds)</td></tr>", 
            gmdate("H:i:s", $this->duration), 
            $this->duration
        );
        printf(
            "<tr><td>Number of unique viewers<td>%d</td></tr>", 
            $this->numberOfLogEntries
        );
        printf(
            "<tr><td>Average total time watched (h:m:s)<td>%s (%ds)</td></tr>", 
            gmdate("H:i:s", $this->averageTimeWatched), 
            $this->averageTimeWatched
        );
        printf("<tr><td>Average of last dropoff time as a percentage of ");
        printf(
            "duration<td>%d%% (%s)</td></tr>",
            $this->averageDropoffTime,
            gmdate("H:i:s", ($this->averageDropoffTime * $this->duration) / 100)
        );
        printf("</table>");
        return true;
    }

    /**
     * Some doc
     * 
     * @param string $email       foo
     * @param string $operootpath override for OPE/OPEtest
     * 
     * @return boolean
     */
    public function listVideos($email, $operootpath)
    {
        $ou = new opeUser();
        $userid = $ou->getUseridByEmail($email);

        $query = sprintf(
            "SELECT DISTINCT pagename FROM VIDEOTAGLOG 
            WHERE userid='%s'", 
            $userid
        );
        // error_log($query);
        $result = $this->conn->query($query);
        if ($result->num_rows) {
            while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
                if ($operootpath == "OPE") {
                    printf(
                        "<p><a href=\"/video-stats?pname=%s&userid=%s\">%s</a>", 
                        $row["pagename"], 
                        $userid, 
                        $row["pagename"]
                    );
                } else {
                    printf(
                        "<p><a href=\"/video-stats_opetest?pname=%s&userid=%s\">%s</a>",
                        $row["pagename"],
                        $userid,
                        $row["pagename"]
                    );
                }
            }
        } else {
            printf("<p>No videos found in the log.</p>\n");
        }
        return $result->num_rows;
    }

    /**
     * Some doc
     * 
     * @param array   $jsonvdata foo
     * @param boolean $OPtagOK   foo
     * 
     * @return boolean
     */
    public function addRecord($jsonvdata, $OPtagOK)
    {
        $userid = $this->conn->real_escape_string($jsonvdata->getUserId());
        $videoName = $this->conn->real_escape_string(
            $jsonvdata->getVideoName()
        );
        $url = $this->conn->real_escape_string($jsonvdata->getURL());
        $contactid = $this->conn->real_escape_string(
            $jsonvdata->getContactID()
        );
        $taglist = $this->conn->real_escape_string(
            implode(" ", $jsonvdata->getTags())
        );

        // if a record exists for this userid,url,contactid then pull 
        // the existing bitmap
        // and merge with that, otherwise start with zeroes.
        $query = sprintf(
            "SELECT HEX(top50PercentViewed),
            HEX(bottom50PercentViewed),
            totalTimeWatched,id FROM VIDEOTAGLOG 
            WHERE userid='%s' AND 
            pagename='%s' AND contactid='%s'", 
            $userid, $videoName, $contactid
        );
        // error_log($query);
        $result = $this->conn->query($query);
        if ($result->num_rows) {
            $row = $result->fetch_array(MYSQLI_ASSOC);
            $hi50 = $row["HEX(top50PercentViewed)"];
            $lo50 = $row["HEX(bottom50PercentViewed)"];
            $initialTotal = $row["totalTimeWatched"];
            $updateRowid = $row["id"];
            $recordExists = true;
            // error_log("Found existing row " . 
            // $updateRowid . " " . $hi50 . " " . $lo50);
        } else {
            $hi50 = "0000000000000000";
            $lo50 = "0000000000000000";
            $recordExists = false;
            $initialTotal = 0;
        }

        $this->psm = new playedSegmentMap($jsonvdata, $hi50, $lo50);

        $finalTotalTimeWatched = max($jsonvdata->getTotaltime(), $initialTotal);
        if (! $recordExists) {
            $query = sprintf(
                "INSERT INTO `VIDEOTAGLOG` (`userid`, `contactid`, `datetime`, 
                `tags`, `url`, `pagename`, `totalTimeWatched`, `duration`,
                `pushedToOntraport`, `top50PercentViewed`, 
                `bottom50PercentViewed`)
                VALUES ('%s', '%s', '%s', '%s', '%s', 
                '%s', %f, %f, '%s', %s, %s)", 
                $userid, 
                $contactid, 
                date("Y-m-d H:i:s"), 
                $taglist, 
                $url, 
                $videoName, 
                $finalTotalTimeWatched, 
                $jsonvdata->getDuration(), 
                $OPtagOK, 
                $this->psm->getTop50String(), 
                $this->psm->getBottom50String()
            );
        } else {
            $query = sprintf(
                "UPDATE `VIDEOTAGLOG` SET `datetime`='%s', 
                `tags`='%s', 
                `totalTimeWatched`=%f, `top50PercentViewed`=%s, 
                `bottom50PercentViewed`=%s 
                WHERE id='%s'", date("Y-m-d H:i:s"), $taglist, 
                $finalTotalTimeWatched, $this->psm->getTop50String(), 
                $this->psm->getBottom50String(), $updateRowid
            );
        }
        // error_log($query);
        $ret = false;
        if ($this->conn->query($query)) {
            $ret = true;
        }
        // error_log("old " . $hi50 . " " . $lo50 . " new " . 
        // $this->_psm->getTop50String() . " " . 
        // $this->_psm->getBottom50String());
        return $ret;
    }

    /**
     * Some doc
     * 
     * @return boolean
     */
    public function shutdown()
    {
        $this->db->shutdown();
        return true;
    }
}
