<?php
/**
 * PlayedSegmentMap -
 * PHP Version 7
 *
 * @category  VVS
 * @package   VVS
 * @author    Brad Dobson <brad@opextras.com>
 * @copyright 2018 Mindful Patterns, LLC - All Rights Reserved
 * @license   http://www.opextras.com/ Proprietary
 * @link      d
 */
//
//
require_once "jsonVideoData.php";
require_once "vvsPercentBits.php";

//
// This class allows us to merge in new played data for an 
// existing video/userid/contactid row
// Take HEX() output from mysql the two bit fields
// Set any new bits according to the played segments in the json 
// data from the browser
//
// TODO reorganize this with jsonData::validate() for more logical structure
//
/**
 * PlayedSegmentMap class
 *
 * @category VVS
 * @package  VVS
 * @author   Brad Dobson <brad@opextras.com>
 * @license  foo http://www.opextras.com
 * @link     d
 */
class PlayedSegmentMap
{
    // phpcs:disable PEAR.NamingConventions.ValidVariableName.PublicUnderscore
    // phpcs:disable PEAR.ControlStructures.MultiLineCondition.StartWithBoolean
    protected $_obj;

    protected $_bits;

    protected $_top50binary;

    protected $_bottom50binary;

    /**
     * Function __construct
     *
     * @param object $jsonObj        json video data object
     * @param string $top50String    top 50 bits
     * @param string $bottom50String bottom 50 bits
     */
    function __construct($jsonObj, $top50String, $bottom50String)
    {
        $this->_obj = $jsonObj->getObject();
        sscanf($top50String, "%16x", $this->_top50binary);
        sscanf($bottom50String, "%16x", $this->_bottom50binary);
        //error_log(sprintf("psm %s %s %016x %016x\n", $top50String, 
        // $bottom50String, $this->_top50binary, $this->_bottom50binary));
        $this->_bits = new vvsPercentBits(
            $this->_top50binary, 
            $this->_bottom50binary
        );
        // For each played segment, set the corresponding percentage bit 
        // if the start or end fits into one of the 100 buckets

        for ($i = 0; $i < $this->_obj->timerange_length; $i ++) {
            $start = "start" . $i;
            $end = "end" . $i;
            for ($j = 1; $j <= 100; $j ++) {
                if (((($this->_obj->duration / 100) * $j) 
                    >= $this->_obj->{"$start"}) 
                    && ((($this->_obj->duration / 100) * $j) 
                    <= $this->_obj->{"$end"})
                ) {
                    // set bit j to 1
                    //printf("%s %f %s %f %d %2.2f\n", $start, 
                    // $this->_obj->{"$start"}, $end, $this->_obj->{"$end"}, 
                    // $j, $this->_obj->duration / 100);
                    $this->_bits->setBitByPercentage($j, 1);
                }
            }
        }
        //error_log(sprintf("psm completed %s %s %016x %016x\n", 
        // $this->_bits->stringify(1), $this->_bits->stringify(0), 
        // $this->_top50binary, $this->_bottom50binary));
    }

    /**
     * Function getTop50String
     *
     * @return string
     */
    public function getTop50String()
    {
        return $this->_bits->stringify(1);
    }

    /**
     * Function getBottom50String
     *
     * @return string
     */
    public function getBottom50String()
    {
        return $this->_bits->stringify(0);
    }
}
?>