<?php
/**
 * GetTagListFromURL - json handler
 * PHP Version 7
 *
 * @category  VVS
 * @package   VVS
 * @author    Brad Dobson <brad@opextras.com>
 * @copyright 2018 Mindful Patterns, LLC - All Rights Reserved
 * @license   http://www.opextras.com/ Proprietary
 * @link      d
 */
require_once 'URLtoTag.php';
$urltag=new URLtoTag();
$urltag->getJSONTagListFromURL($_POST);
?>