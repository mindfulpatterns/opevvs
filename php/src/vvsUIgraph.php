<?php
/**
 * VvsUIGraph -
 * PHP Version 7
 *
 * @category  VVS
 * @package   VVS
 * @author    Brad Dobson <brad@opextras.com>
 * @copyright 2018 Mindful Patterns, LLC - All Rights Reserved
 * @license   http://www.opextras.com/ Proprietary
 * @link      d
 */
// This file is used as php snippet that is included on a wordpress page
// to display a chart and all statistics about a specific video
//
// TODO pull more stats and info
//
require_once 'videoTagLog.php';
$vtl = new videoTagLog();

// phpcs:disable Generic.Files.LineLength.TooLong
?>
<!--This is almost verbatim from the google charts example-->
<!--Load the AJAX API-->
<script type="text/javascript"
    src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">

      // Load the Visualization API and the corechart package.
      google.charts.load('current', {'packages':['corechart']});

      // Set a callback to run when the Google Visualization API is loaded.
      google.charts.setOnLoadCallback(drawChart);

      // Callback that creates and populates a data table,
      // instantiates the pie chart, passes in the data and
      // draws it.
      function drawChart() {

        // Create the data table.
        var data = new google.visualization.DataTable();
        data.addColumn('number', 'Viewers');
        data.addColumn('number', 'Viewed Segment (1/100th duration)');
        data.addRows([
            <?php $vtl->retrieveEntries(); ?>
        ]);

        // Set chart options
        var options = 
            {'title':'Number of viewers per percentage of video length:\n<?php echo $_GET["pname"]; ?>',
                       'width':800,
                       'height':600};

        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.ColumnChart(
                document.getElementById('chart_div'));
        chart.draw(data, options);
      }
    </script>

<!--Div that will hold the pie chart-->
<div id="chart_div"></div>

<div><?php $vtl->showStats();?></div>
