<?php
/**
 * New_data -
 * PHP Version 7
 *
 * @category  VVS
 * @package   VVS
 * @author    Brad Dobson <brad@opextras.com>
 * @copyright 2018 Mindful Patterns, LLC - All Rights Reserved
 * @license   http://www.opextras.com/ Proprietary
 * @link      d
 */
// Main handler for incoming video view data from user pages
// Doesn't return a response since some ios played segment maps come 
// on javascript pagehide with synchronous=false

require_once 'videoDataHandler.php';

header('Access-Control-Allow-Origin: *');
$vdh = new videoDataHandler();
$vdh->handleData($_POST);
?>