<?php
/**
 * JsonVideoData -
 * PHP Version 7
 *
 * @category  VVS
 * @package   VVS
 * @author    Brad Dobson <brad@opextras.com>
 * @copyright 2018 Mindful Patterns, LLC - All Rights Reserved
 * @license   http://www.opextras.com/ Proprietary
 * @link      d
 */

require_once 'URLtoTag.php';

/**
 * JsonVideoData class
 *
 * @category VVS
 * @package  VVS
 * @author   Brad Dobson <brad@opextras.com>
 * @license  foo http://www.opextras.com
 * @link     d
 */
class JsonVideoData
{

    // Incoming data structure is
    // int contact_id - ID of the contact playing the video
    // float duration - duration of the video
    // int timerange_length - number of elements in the list of played segments
    // string userid - unique userid in users table
    // float start0 - played segment 0 start time index
    // float end0 - played segment 0 end time index
    // ... start1/end1 ... - remaining played segments
    protected $obj;

    protected $url;

    protected $videoName;

    protected $tags;

    protected $totaltime;
    
    protected $urltotag;

    /**
     * Constructor
     * 
     * @param arrary $inputarr $_POST or uts array
     */
    function __construct($inputarr)
    {
        if (!defined('PHPUNIT_COMPOSER_INSTALL') 
            && !defined('__PHPUNIT_PHAR__')
        ) {
            header("Content-Type: application/json; charset=UTF-8");
        }
        $this->obj = json_decode($inputarr["x"], false);
        //error_log(print_r($this->obj,true));
        $this->urltotag = new URLtoTag();
    }

    /**
     * Validate
     * 
     * @return boolean
     */
    function validate()
    {
        $ret = true;
        if (! isset($this->obj->userid) || strlen($this->obj->userid) == 0) {
            $ret = false;
        }
        if (! isset($this->obj->timerange_length) 
            || $this->obj->timerange_length <= 0
        ) {
            $ret = false;
        }
        if (! isset($this->obj->contact_id) 
            || ! is_numeric($this->obj->contact_id)
        ) {
            $ret = false;
        }
        if (! isset($this->obj->timerange_length) 
            || ! is_numeric($this->obj->timerange_length)
        ) {
            $ret = false;
        }
        if ($ret == true) {
            $this->url = strtok($_SERVER['HTTP_REFERER'], "?");

            $this->videoName = $this->urltotag->getVideoNameFromURL($this->url);
            $segments = $this->obj->timerange_length;
            $this->tags = array();
            $this->totaltime = 0;
            // For each played segment, add a tag if the start or end 
            // fits into one of the 10 buckets
            for ($i = 0; $i < $segments; $i ++) {
                for ($j = 0; $j < 10; $j ++) {
                    $value = $this->urltotag->buildTag(
                        $this->videoName, 
                        ($j * 10),
                        (($j + 1) * 10)
                    );
                    $entries = array(
                        "start" . $i,
                        "end" . $i
                    );
                    if (! isset($this->obj->{"start" . $i}) 
                        || ! isset($this->obj->{"end" . $i})
                    ) {
                        return false;
                    }
                    // if start in j..j+1
                    // if end in j..j+1
                    // if start<=j && end >=j+1
                    foreach ($entries as $entry) {
                        // phpcs:disable PEAR.ControlStructures.MultiLineCondition.StartWithBoolean
                        if (($this->obj->{"$entry"} 
                            >= ($this->obj->duration / 10) * $j) 
                            && ($this->obj->{"$entry"} 
                            <= ($this->obj->duration / 10) * ($j + 1))
                        ) {

                            if (! in_array($value, $this->tags, true)) {

                                array_push($this->tags, $value);
                            }
                        }
                    }
                    if (($this->obj->{"$entries[0]"} 
                        <= ($this->obj->duration / 10) * $j) 
                        && ($this->obj->{"$entries[1]"} 
                        >= ($this->obj->duration / 10) * ($j + 1))
                    ) {
                    // phpcs:enable PEAR.ControlStructures.MultiLineCondition.StartWithBoolean
                        
                        if (! in_array($value, $this->tags, true)) {

                            array_push($this->tags, $value);
                        }
                    }
                }
            }
            for ($i = 0; $i < $segments; $i ++) {
                $this->totaltime += $this->obj->{"end" . $i} 
                    - $this->obj->{"start" . $i};
            }
        }
        return ($ret);
    }

    /**
     * GetURL
     * 
     * @return string
     */
    public function getURL()
    {
        return $this->url;
    }

    /**
     * GetvideoName
     *
     * @return string
     */
    public function getvideoName()
    {
        return $this->videoName;
    }

    /**
     * GetTags
     *
     * @return string
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * GetContactID
     *
     * @return string
     */
    public function getContactID()
    {
        return $this->obj->contact_id;
    }

    /**
     * GetUserID
     *
     * @return string
     */
    public function getUserID()
    {
        return $this->obj->userid;
    }

    /**
     * GetDuration
     *
     * @return string
     */
    public function getDuration()
    {
        return $this->obj->duration;
    }

    /**
     * GetTotaltime
     *
     * @return string
     */
    public function getTotaltime()
    {
        return $this->totaltime;
    }

    /**
     * GetTotaltime
     *
     * @return string
     */
    public function getObject()
    {
        return $this->obj;
    }
}
?>