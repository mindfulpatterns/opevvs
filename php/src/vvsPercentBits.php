<?php
/**
 * VvsPercentBits -
 * PHP Version 7
 *
 * @category  VVS
 * @package   VVS
 * @author    Brad Dobson <brad@opextras.com>
 * @copyright 2018 Mindful Patterns, LLC - All Rights Reserved
 * @license   http://www.opextras.com/ Proprietary
 * @link      d
 */
// Given 2 50-bit binary hex strings that can be used with MySQL binary,
// set the Nth bit where N=1..100% of video viewed. Split over 2 numbers due
// to 64-bit max size.
//
// UPDATE `videoTagLog` SET `b1` = 0x02000000000002 WHERE `videoTagLog`.`id` = 1;
/**
 * VvsPercentBits class
 *
 * @category VVS
 * @package  VVS
 * @author   Brad Dobson <brad@opextras.com>
 * @license  foo http://www.opextras.com
 * @link     d
 */
class VvsPercentBits
{
    // phpcs:disable PEAR.NamingConventions.ValidVariableName.PublicUnderscore
    
    protected $_top50binary;

    protected $_bottom50binary;

    /**
     * Function __construct
     *
     * @param int $top    top 50 bits
     * @param int $bottom bottom 50 bits
     */
    public function __construct($top, $bottom)
    {
        $this->_top50binary = $top;
        $this->_bottom50binary = $bottom;
    }

    /**
     * Function __construct
     *
     * @param int $percentage percentage bit to set
     * @param int $val        1 or zero
     * 
     * @return none
     */
    function setBitByPercentage($percentage, $val)
    {
        // If 0, and it with its negation
        if ($val) {
            if ($percentage >= 50) {
                $this->_top50binary |= $val << ($percentage - 50);
            } else {
                $this->_bottom50binary |= $val << ($percentage - 1);
            }
        } else {
            if ($percentage >= 50) {
                $this->_top50binary &= ~(1 << ($percentage - 50));
            } else {
                $this->_bottom50binary &= ~(1 << ($percentage - 1));
            }
        }
    }

    /**
     * Function __construct
     *
     * @param int $percentage percentage bit to get
     * 
     * @return int
     */
    function getBit($percentage)
    {
        if ($percentage >= 50) {
            $ret = ($this->_top50binary &= 1 << ($percentage - 50)) > 0;
        } else {
            $ret = ($this->_bottom50binary &= 1 << ($percentage - 1)) > 0;
        }
        return ($ret);
    }

    /**
     * Function stringify
     *
     * @param bool $istop is top or bottom section
     *
     * @return int
     */
    function stringify($istop)
    {
        $ret = "";
        if ($istop) {
            $ret = sprintf('0x%016x', $this->_top50binary);
        } else {
            $ret = sprintf('0x%016x', $this->_bottom50binary);
        }
        // printf("psm string %s %s\n",$istop, $ret);
        return ($ret);
    }
}
?>