<?php
/**
 * VvsUIListVideos -
 * PHP Version 7
 *
 * @category  VVS
 * @package   VVS
 * @author    Brad Dobson <brad@opextras.com>
 * @copyright 2018 Mindful Patterns, LLC - All Rights Reserved
 * @license   http://www.opextras.com/ Proprietary
 * @link      d
 */
require_once 'videoTagLog.php';
global $current_user;
get_currentuserinfo();
$vtl = new videoTagLog();
// Allow OPEtest override
if (!isset($operootpath)) {
    $operootpath="OPE";
}
$vtl->listVideos($current_user->user_login, $operootpath);
?>
