<?php
/**
 * VvsUISetup -
 * PHP Version 7
 *
 * @category  VVS
 * @package   VVS
 * @author    Brad Dobson <brad@opextras.com>
 * @copyright 2018 Mindful Patterns, LLC - All Rights Reserved
 * @license   http://www.opextras.com/ Proprietary
 * @link      d
 */

require_once 'autoload.php';
$ou = new opeUser();
global $current_user;
get_currentuserinfo();
$ou->getUseridByEmail($current_user->user_login);
// Allow OPEtest override
if (!isset($operootpath)) {
    $operootpath="OPE";
}
//phpcs:disable Generic.Files.LineLength.TooLong
?>
<script id="opextras_vvs_uijs" src="/<?php echo $operootpath; ?>/OPEVVS/js/vvs_ui.js?ver=v2"></script>

<div id="email" style="display: none"><?php echo $current_user->user_login; ?></div>
<div id="hashthingy" style="display: none"><?php echo $ou->getHashedID(); ?></div>

<p>Your OPE UserID is <b><?php echo $ou->getUserid();?></b></p>
<h2>Step 1: Grab a new appid/apikey pair from your Ontraport account
    just for opextras.com</h2>
<p>
    It's nice to have an appid/apikey pair for each integration so that you
    are able to localize any issues. Hopefully that won't occur with
    opextras.com! Anyways, bop over to your Ontraport account and get
    something you can plug in to the next step. Here are the <a
     href="https://support.ontraport.com/hc/en-us/articles/217882248#how-to-obtain-an-api-key">Ontraport
        instructions</a> to obtain those values.
<h2>Step 2: Input your appid/apikey</h2>
<div>
    <div id="inputelements">
        <p>Application ID from Ontraport</p>
        <input type="text" id="appid"
            onchange="updateUserInformation(this.id, this.value)" value="not set">
        <img id="spinner1" style="display: none; float: right"
            src="/<?php echo $operootpath; ?>/OPEVVS/html/media/Spinner-1s-43px.gif"></img>
        <p>API Key from Ontraport</p>
        <input type="text" id="apikey"
            onchange="updateUserInformation(this.id, this.value)" value="not set">
        <h2>Step 3: Verify connection with Ontraport</h2>
        <input type="button" id="verifybutton"
            style="background-color: #f5f5f5"
            value="Verify appid/apikey with Ontraport" onclick="OPauthCheck()">
        <p>
            &nbsp;<span id="authcheck"></span>
        </p>
    </div>
    <h2>Step 4: Add this code snippet to your video page headers</h2>

    <p>Copy this into your video page headers just like you do for facebook
        pixels and google analytics. For an ONTRApage, click the Settings icon
        and insert the code into Custom header code in the Custom Code
        section.</p>
    <p>💡Only skip this step if you do not want the feature to work.</p>
    <p style="background-color: #e0e0e0" id="jsheader">loading</p>

    <input type="button" id="button1" style="background-color: #f5f5f5"
        onclick="CopyToClipboard('jsheader')" value="Click to copy">

    <h2>Step 5: Optionally pre-create the tags for your video(s)</h2>
    <p>If you don't take this step tags will be automatically generated as
        viewers reach specific parts of your videos. You do want to take this
        step if you are coding a campaign and want to have the automation
        ready based on those tag changes.</p>

    <p>Here's an example:</p>
    <p>
        Your video page URL is <br>"http://you-are-awesome.xy/watch-my-video-1.html"
    </p>
    <p>
        opextras uses the page part of the URL to generate tags: <br>"watch-my-video-1.html"
    </p>
    <p>Enter your URL and I'll display the resulting tags:</p>
    <input type="text" id="tagurl" onchange="mktags(this.id, this.value)"
        value=""> <br>
    <p style="background-color: #f0f0f0" id="list_of_tags"></p>




    <h2>Please note the following known limitations in the beta software</h2>
    <ul>
        <li>Viewers that have been cookied by Ontraport will be tagged using a
            combination of the page name and the 10% increment they watched, <br>e.g.
            <br>watch-my-video-1.html_0_10<br>watch-my-video-1.html_10_20 <br>..<br>watch-my-video-1.html_90_100

        
        
        <li>Data is only collected for the first video on a page
        
        <li>The normal Ontraport API rate-limiting applies to tagging: if you
            have > 180 video viewers pause/stop their video in one minute then
            the system will be throttled (gracefully, I think). That's a hard
            thing to test for and to understand behavior, but I'll give it a
            shot.
        
        <li>Changing the length of your video (e.g. you upload a new version)
            <i>should</i> be OK for the statistics since they are
            percentage-based. If you have automation based on a tag at a specific
            10% interval, you could break that if the video duration changes
            enough to change which 10% interval is the important one.
    
    </ul>
    <div></div>


    <div style="height: 90px">
        <img id="vvsspinner" style="display: none"
            src="/<?php echo $operootpath; ?>/OPEVVS/html/media/Spinner-1s-43px.gif"></img>
    </div>
</div>


