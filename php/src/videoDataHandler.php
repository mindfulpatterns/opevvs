<?php
/**
 * VideoDataHandler -
 * PHP Version 7
 *
 * @category  VVS
 * @package   VVS
 * @author    Brad Dobson <brad@opextras.com>
 * @copyright 2018 Mindful Patterns, LLC - All Rights Reserved
 * @license   http://www.opextras.com/ Proprietary
 * @link      d
 */

// Main handler for incoming video view data from user pages
// Doesn't return a json response since some ios played segment maps come on 
// javascript pagehide with synchronous=false
require_once 'autoload.php';

/**
 * VideoDataHandler class
 *
 * @category VVS
 * @package  VVS
 * @author   Brad Dobson <brad@opextras.com>
 * @license  foo http://www.opextras.com
 * @link     d
 */
class VideoDataHandler
{

    protected $videoData;

    protected $op;

    /**
     * Function handleData
     * 
     * @param array $inputarr $_POST or uts array
     * 
     * @return string
     */
    public function handleData($inputarr)
    {
        $ret = false;
        $this->videoData = new jsonVideoData($inputarr);
        $this->op = new OntraportFunctions();
        if ($this->videoData->validate()) { 
            // Don't do anything if there aren't any played segments, etc.
            $ou = new opeUser();
            if ($ou->getUserByUSERID($this->videoData->getUserID())) { 
                // Go ahead on valid userid
                $tags = $this->videoData->getTags();
                $ret = $this->op->addTags(
                    $ou->getAppid(), $ou->getApiKey(), 
                    $this->videoData->getContactID(), $tags
                );

                // TODO recheck logic related to addTags failure...
                if ($ret == true) {
                    $vtl = new videoTagLog();
                    $vtl->addRecord($this->videoData, $ret);
                }
            } else {
                $ret=false;
                //error_log($this->videoData->getUserID() . " NOT FOUND");
            }
            $ou->shutdown();
        } else {
            $ret = false;
        }
        return $ret;
    }
}
?>