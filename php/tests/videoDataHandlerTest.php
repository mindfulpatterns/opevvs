<?php
declare(strict_types = 1);

require_once 'src/videoDataHandler.php';

use PHPUnit\Framework\TestCase;

final class videoDataHandlerTest extends TestCase
{

    protected $vdh;

    public function testdataHandlerOK()
    {
        $this->vdh = new videoDataHandler();
        $_SERVER['HTTP_REFERER'] = "http://localhost:8888/VVS/html/videotest3.html";
        $inputarr=array(
            "x" => '{"contact_id":"1","duration":84.822494,"timerange_length":"2","userid":"FARBLE129","start0":0,"end0":2.483251,"start1":30,"end1":40.483251}'
        );
        $this->assertEquals(true, $this->vdh->handleData($inputarr));
    }

    // Test tagging fail
    public function testdataHandlerTaggingFail()
    {
        $this->vdh = new videoDataHandler();
        $_SERVER['HTTP_REFERER'] = "http://localhost:8888/VVS/html/videotest3.html";
        $inputarr=array(
            "x" => '{"contact_id":"1","duration":84.822494,"timerange_length":"0","userid":"foofoo","start0":0,"end0":2.483251,"start1":30,"end1":40.483251}'
        );
        $this->assertEquals(false, $this->vdh->handleData($inputarr));
    }
    
    // Test invalid data
    public function testdataHandlerInvalidData()
    {
        $this->vdh = new videoDataHandler();
        $_SERVER['HTTP_REFERER'] = "http://localhost:8888/VVS/html/videotest3.html";
        $inputarr=array(
            "x" => '{"contact_id":"1","duration":84.822494,"timerange_length":"0","userid":"FARBLEXXX","start0":0,"end0":2.483251,"start1":30,"end1":40.483251}'
        );
        $this->assertEquals(false, $this->vdh->handleData($inputarr));
    }
    
    // Test userid not found
    public function testdataHandlerUseridNotFound()
    {
        $this->vdh = new videoDataHandler();
        $_SERVER['HTTP_REFERER'] = "http://localhost:8888/VVS/html/videotest3.html";
        $inputarr=array(
            "x" => '{"contact_id":"1","duration":84.822494,"timerange_length":"2","userid":"FARBLEXXX","start0":0,"end0":2.483251,"start1":30,"end1":40.483251}'
        );
        $this->assertEquals(false, $this->vdh->handleData($inputarr));
    }
}
?>