<?php
declare(strict_types = 1);

require_once 'src/URLtoTag.php';
require_once 'tests/curlPost.php';

use PHPUnit\Framework\TestCase;

final class URLtoTagTest extends TestCase
{

    protected $ut;

    public function testgetVideoNameFromURLNoParams()
    {
        $this->ut = new URLtoTag();
        $this->assertEquals('videotest3.html', $this->ut->getVideoNameFromURL("http://localhost:8888/FOOVVS/html/videotest3.html"));
    }

    public function testgetVideoNameFromURLWithParams()
    {
        $this->ut = new URLtoTag();
        $this->assertEquals('videotest3.html', $this->ut->getVideoNameFromURL("http://localhost:8888/FOOVVS/html/videotest3.html?a=b&c=d"));
    }

    public function testbuildTag()
    {
        $this->ut = new URLtoTag();
        $this->assertEquals('videotest3.html_10_20', $this->ut->buildTag("videotest3.html", 10, 20));
    }

    public function testgetJSONTagListFromURL()
    {
        $this->ut = new URLtoTag();
        $inputarr=array(
            "x" => '{"url":"http://localhost:8888/FOOVVS/html/videotest3.html?a=b&c=d"}'
        );
        $this->ut->getJSONTagListFromURL($inputarr);
        $this->expectOutputString('{"tags":"<br>videotest3.html_0_10<br>videotest3.html_10_20<br>videotest3.html_20_30<br>videotest3.html_30_40<br>videotest3.html_40_50<br>videotest3.html_50_60<br>videotest3.html_60_70<br>videotest3.html_70_80<br>videotest3.html_80_90<br>videotest3.html_90_100"}');
    }
}
?>