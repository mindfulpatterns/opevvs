<?php
declare(strict_types = 1);

require_once 'src/jsonVideoData.php';

use PHPUnit\Framework\TestCase;

/**
 * jsonVideoData test case.
 */
final class jsonVideoDataTest extends TestCase
{

    protected $jsonVideoData;

    protected function normalSetup()
    {
        $_SERVER['HTTP_REFERER'] = "http://localhost:8888/VVS/html/videotest3.html";
        $inputarr=array(
            "x" => '{"contact_id":"1","duration":84.822494,"timerange_length":"2","userid":"FARBLE129","start0":0,"end0":2.483251,"start1":30,"end1":40.483251}'
            );
        
        $this->jsonVideoData = new jsonVideoData($inputarr);
        $this->jsonVideoData->validate();
    }

    public function testCanConstructOK()
    {
        $_SERVER['HTTP_REFERER'] = "http://localhost:8888/VVS/html/videotest3.html";

        $inputarr=array(
            "x" => '{"contact_id":"1","duration":84.822494,"timerange_length":2,"userid":"FARBLE129","start0":0,"end0":2.483251,"start1":30,"end1":40.483251}'
        );        
        $this->jsonVideoData = new jsonVideoData($inputarr);
        $this->assertEquals("FARBLE129", $this->jsonVideoData->getUserID());
    }

    /**
     * Tests jsonVideoData->validate()
     */
    public function testValidateHappyPath()
    {
        $_SERVER['HTTP_REFERER'] = "http://localhost:8888/VVS/html/videotest3.html";
        $inputarr=array(
            "x" => '{"contact_id":"1","duration":84.822494,"timerange_length":2,"userid":"FARBLE129","start0":0,"end0":2.483251,"start1":30,"end1":40.483251}'
        );
        
        $this->jsonVideoData = new jsonVideoData($inputarr);
        $this->assertEquals(true, $this->jsonVideoData->validate());
    }

    public function testValidateBadUserid()
    {
        $_SERVER['HTTP_REFERER'] = "http://localhost:8888/VVS/html/videotest3.html";
        $inputarr=array(
            "x" => '{"contact_id":"1","duration":84.822494,"timerange_length":2,"userid":"","start0":0,"end0":2.483251,"start1":30,"end1":40.483251}'
        );
        $this->jsonVideoData = new jsonVideoData($inputarr);
        $this->assertEquals(false, $this->jsonVideoData->validate());
        $inputarr=array(
            "x" => '{"contact_id":"1","duration":84.822494,"timerange_length":2,"start0":0,"end0":2.483251,"start1":30,"end1":40.483251}'
        );        
        $this->jsonVideoData = new jsonVideoData($inputarr);
        $this->assertEquals(false, $this->jsonVideoData->validate());
    }

    public function testValidateBadContactid()
    {
        $_SERVER['HTTP_REFERER'] = "http://localhost:8888/VVS/html/videotest3.html";
        $inputarr=array(
            "x" => '{"duration":84.822494,"timerange_length":2,"start0":0,"end0":2.483251,"start1":30,"end1":40.483251}'
        );        
        $this->jsonVideoData = new jsonVideoData($inputarr);
        $this->assertEquals(false, $this->jsonVideoData->validate());
         $inputarr=array(
            "x" => '{"contact_id":"abc","duration":84.822494,"timerange_length":2,"start0":0,"end0":2.483251,"start1":30,"end1":40.483251}'
        );
        $this->jsonVideoData = new jsonVideoData($inputarr);
        $this->assertEquals(false, $this->jsonVideoData->validate());
    }

    public function testValidateBadTimerange_length()
    {
        $_SERVER['HTTP_REFERER'] = "http://localhost:8888/VVS/html/videotest3.html";

        $inputarr=array(
            "x" => '{"contact_id":"1","duration":84.822494,"timerange_length":"abc","userid":"FARBLE129","start0":0,"end0":2.483251,"start1":30,"end1":40.483251}'
        );
        $this->jsonVideoData = new jsonVideoData($inputarr);
        $this->assertEquals(false, $this->jsonVideoData->validate());
        $inputarr=array(
            "x" => '{"contact_id":"1","duration":84.822494,"timerange_length":"abc","userid":"FARBLE129","start0":0,"end0":2.483251,"start1":30,"end1":40.483251}'
        );
        $this->jsonVideoData = new jsonVideoData($inputarr);
        $this->assertEquals(false, $this->jsonVideoData->validate());
    }

    /**
     * Tests jsonVideoData->getvideoName()
     */
    public function testGetvideoNameBasic()
    {
        $this->normalSetup();
        $this->assertEquals("videotest3.html", $this->jsonVideoData->getvideoName());
    }

    public function testGetvideoNameWithParams()
    {
        $_SERVER['HTTP_REFERER'] = "http://localhost:8888/VVS/html/videotest3.html?a=b&c=d";
        $inputarr=array(
            "x" => '{"contact_id":"1","duration":84.822494,"timerange_length":2,"userid":"FARBLE129","start0":0,"end0":2.483251,"start1":30,"end1":40.483251}'
        );        
        $this->jsonVideoData = new jsonVideoData($inputarr);
        $this->jsonVideoData->validate();
        $this->assertEquals("videotest3.html", $this->jsonVideoData->getvideoName());
    }

    /**
     * Tests jsonVideoData->getContactID()
     */
    public function testGetContactID()
    {
        $this->normalSetup();
        $this->assertEquals("1", $this->jsonVideoData->getContactID());
    }

    /**
     * Tests jsonVideoData->getUserID()
     */
    public function testGetUserID()
    {
        $this->normalSetup();
        $this->assertEquals("FARBLE129", $this->jsonVideoData->getUserID());
    }

    public function testGetDuration()
    {
        $this->normalSetup();
        $this->assertEquals(84.822494, $this->jsonVideoData->getDuration());
    }

    public function testGetURL()
    {
        $this->normalSetup();
        $this->assertEquals($_SERVER['HTTP_REFERER'], $this->jsonVideoData->getURL());
    }

    public function testGetObject()
    {
        $this->normalSetup();
        $this->assertEquals(1, $this->jsonVideoData->getObject()->contact_id);
    }

    public function testGetTotaltime()
    {
        $this->normalSetup();
        $this->assertEquals(12.966502, $this->jsonVideoData->getTotaltime());
    }

    public function testGetTagsNormal()
    {
        $this->normalSetup();
        $arr = $this->jsonVideoData->getTags();
        $this->assertEquals("videotest3.html_0_10", $arr[0]);
        $this->assertEquals("videotest3.html_30_40", $arr[1]);
        $this->assertEquals("videotest3.html_40_50", $arr[2]);
        $this->assertEquals(3, count($arr));
    }
    
    public function testGetTags50Precent()
    {
        $_SERVER['HTTP_REFERER'] = "http://localhost:8888/VVS/html/videotest3.html";
        $inputarr=array(
            "x" => '{"contact_id":"1","duration":84.822494,"timerange_length":"1","userid":"FARBLE129","start0":0,"end0":42.0}'
        );
        $this->jsonVideoData = new jsonVideoData($inputarr);
        $this->jsonVideoData->validate();
        $arr = $this->jsonVideoData->getTags();
        $this->assertEquals("videotest3.html_0_10", $arr[0]);
        $this->assertEquals("videotest3.html_40_50", $arr[4]);
        $this->assertEquals(5, count($arr));
    }
    
    public function testGetTagsAll()
    {
        $_SERVER['HTTP_REFERER'] = "http://localhost:8888/VVS/html/videotest3.html";
        $inputarr=array(
            "x" => '{"contact_id":"1","duration":84.822494,"timerange_length":"1","userid":"FARBLE129","start0":0,"end0":85.0}'
        );
        $this->jsonVideoData = new jsonVideoData($inputarr);
        $this->jsonVideoData->validate();
        $arr = $this->jsonVideoData->getTags();
        $this->assertEquals("videotest3.html_0_10", $arr[0]);
        $this->assertEquals("videotest3.html_90_100", $arr[9]);
        $this->assertEquals(10, count($arr));
    }
    public function testGetTagsBogus1()
    {
        $_SERVER['HTTP_REFERER'] = "http://localhost:8888/VVS/html/videotest3.html";
        $inputarr=array(
            "x" => '{"contact_id":"1","duration":84.822494,"timerange_length":"1","userid":"FARBLE129","start0":85.0,"end0":86.0}'
        );
        $this->jsonVideoData = new jsonVideoData($inputarr);
        $this->jsonVideoData->validate();
        $arr = $this->jsonVideoData->getTags();
        $this->assertEquals(0, count($arr));
    }
    public function testGetTagsBorkedTimeRange()
    {
        $_SERVER['HTTP_REFERER'] = "http://localhost:8888/VVS/html/videotest3.html";
        $inputarr=array(
            "x" => '{"contact_id":"1","duration":84.822494,"timerange_length":"10","userid":"FARBLE129","start0":85.0,"end0":86.0}'
        );
        $this->jsonVideoData = new jsonVideoData($inputarr);
        $this->assertEquals(false,$this->jsonVideoData->validate());
    }
}

