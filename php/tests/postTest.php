<?php

// Drive server-side application using posts
// Tests everything except client-side
declare(strict_types = 1);

use PHPUnit\Framework\TestCase;

final class postTest extends TestCase
{

    protected $url;

    protected $data;

    public function doPost()
    {
        $url = 'http://localhost:8888/OPE/OPEVVS/php/src/getTagListFromURL.php';
        $data = array('url' => 'http://localhost:8888/OPE/OPEVVS/php/src/somevideo.html?a=b&c=d');
        $content = json_encode($data);
         
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER,
            array("Content-type: application/json"));
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $content);
        
        $json_response = curl_exec($curl);
        
        $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        
        if ( $status != 200 ) {
            die("Error: call to URL $url failed with status $status, response $json_response, curl_error " . curl_error($curl) . ", curl_errno " . curl_errno($curl));
        }
        
        curl_close($curl);
        
        $response = json_decode($json_response, true);
        return ($response);
    }

    public function testgetTagListFromURL()
    {
        $this->url = 'http://localhost:8888/OPE/OPEVVS/php/src/getTagListFromURL.php';
        $this->data = array(
            'x' => 'url=http://localhost:8888/OPE/OPEVVS/php/src/somevideo.html?a=b&c=d'
        );
        if (false) {
        $result = $this->doPost();
        $this->assertNotEquals(FALSE, $result);
        $this->assertStringStartsWith("<br>somevideo.html_0_10", $result["tags"]);
        $this->assertStringEndsWith("somevideo.html_90_100", $result["tags"]);
        } else {
            $this->assertEquals(1,1);
            
        }
    }
    
    
}
?>