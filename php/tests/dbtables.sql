-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Aug 07, 2018 at 05:22 PM
-- Server version: 5.6.38
-- PHP Version: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `OPEXTRAS`
--

-- --------------------------------------------------------

--
-- Table structure for table `USERS`
--

CREATE TABLE `USERS` (
  `email` text NOT NULL,
  `firstname` text NOT NULL,
  `lastname` text NOT NULL,
  `appid` text NOT NULL,
  `apikey` text NOT NULL,
  `userid` text NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `USERS`
--

INSERT INTO `USERS` (`email`, `firstname`, `lastname`, `appid`, `apikey`, `userid`, `id`) VALUES
('brad@bdobson.net', 'Brad', 'Dobson', 'faefafefddee', 'eAO2Ow13hRDSQYPqq', 'FARBLE129', 1),
('bradford.dobson@gmail.com', '', '', '2_95191_b5lnNZKVz', 'eAO2Ow13hRDSQYP', 'OPEXTRAS_5b5e0c01e5554', 9),
('admin', '', '', 'not set', 'not set', 'OPEXTRAS_5b61e77b94e7f', 10),
('', '', '', 'not set', 'not set', 'OPEXTRAS_5b68973533996', 11);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `USERS`
--
ALTER TABLE `USERS`
  ADD UNIQUE KEY `id` (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `USERS`
--
ALTER TABLE `USERS`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;


-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Aug 07, 2018 at 05:22 PM
-- Server version: 5.6.38
-- PHP Version: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `OPEXTRAS`
--

-- --------------------------------------------------------

--
-- Table structure for table `videoTagLog`
--

CREATE TABLE `videoTagLog` (
  `userid` text NOT NULL,
  `contactid` int(11) NOT NULL,
  `datetime` datetime NOT NULL,
  `tags` text NOT NULL,
  `url` text NOT NULL,
  `pagename` text NOT NULL,
  `totalTimeWatched` float NOT NULL,
  `duration` float NOT NULL,
  `pushedToOntraport` tinyint(1) NOT NULL,
  `top50PercentViewed` binary(8) NOT NULL,
  `bottom50PercentViewed` binary(8) NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `videoTagLog`
--

INSERT INTO `videoTagLog` (`userid`, `contactid`, `datetime`, `tags`, `url`, `pagename`, `totalTimeWatched`, `duration`, `pushedToOntraport`, `top50PercentViewed`, `bottom50PercentViewed`, `id`) VALUES
('OPEXTRAS_5b5e0c01e5554', 1, '2018-08-06 22:22:36', 'videotest3.html_40_50', 'http://localhost:8888/VVS/html/videotest3.html', 'videotest3.html', 75.4833, 84.8225, 1, 0x0007fffe003fffff, 0x0001ffffffffffff, 20),
('OPEXTRAS_5b5e0c01e5554', 1, '2018-08-06 22:22:36', 'videotest3.html_20_30 videotest3.html_30_40 videotest3.html_70_80', 'http://localhost:8888/FOOVVS/html/videotest3.html', 'videotest3.html', 20.9665, 84.8225, 1, 0x0000000000000000, 0x00000007ff800fff, 21),
('OPEXTRAS_5b5e0c01e5554', 2, '2018-08-06 22:32:05', 'videotest3.html_40_50', 'http://localhost:8888/VVS/html/videotest3.html', 'videotest3.html', 75.4833, 84.8225, 1, 0x0007fffe003fffff, 0x0001ffffffffffff, 22),
('OPEXTRAS_5b5e0c01e5554', 2, '2018-08-06 22:32:05', 'videotest3.html_20_30 videotest3.html_30_40 videotest3.html_70_80', 'http://localhost:8888/FOOVVS/html/videotest3.html', 'videotest3.html', 20.9665, 84.8225, 1, 0x0000000000000000, 0x00000007ff800fff, 23);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `videoTagLog`
--
ALTER TABLE `videoTagLog`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `videoTagLog`
--
ALTER TABLE `videoTagLog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
