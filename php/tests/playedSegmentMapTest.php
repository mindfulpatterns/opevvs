<?php
declare(strict_types = 1);

require_once 'src/playedSegmentMap.php';

use PHPUnit\Framework\TestCase;

/**
 * vvsSiteVariables test case.
 */
final class playedSegmentMapTest extends TestCase
{

    protected $psm;

    protected $jsonVideoData;

    public function testCanConstructOK()
    {
        $_SERVER['HTTP_REFERER'] = "http://localhost:8888/VVS/html/videotest3.html";
        $inputarr=array(
            "x" => '{"contact_id":"1","duration":84.822494,"timerange_length":"2","userid":"FARBLE129","start0":0,"end0":2.483251,"start1":30,"end1":40.483251}'
        );
        $this->jsonVideoData = new jsonVideoData($inputarr);
        $this->jsonVideoData->validate();

        $this->psm = new playedSegmentMap($this->jsonVideoData, "0x00040f0000ff000f", "0x0000000b0000000a");

        $this->assertNotEquals(null, $this->psm);
    }

    public function testGetStrings()
    {
        $_SERVER['HTTP_REFERER'] = "http://localhost:8888/VVS/html/videotest3.html";
        // Set bottom and top percentage bits
        $inputarr=array(
            "x" => '{"contact_id":"1","duration":84.822494,"timerange_length":"2","userid":"FARBLE129","start0":0,"end0":1,"start1":84,"end1":85}'
        );
        $this->jsonVideoData = new jsonVideoData($inputarr);
        $this->jsonVideoData->validate();

        $this->psm = new playedSegmentMap($this->jsonVideoData, "0x00040f0000ff000f", "0x0000000b0000000a");

        $this->assertEquals("0x0004040f0000ff00", $this->psm->getTop50String());
        $this->assertEquals("0x000000000b000001", $this->psm->getBottom50String());
    }

    public function testFullSet()
    {
        $_SERVER['HTTP_REFERER'] = "http://localhost:8888/VVS/html/videotest3.html";
        // Set all percentage bits
        $inputarr=array(
            "x" => '{"contact_id":"1","duration":84.822494,"timerange_length":"1","userid":"FARBLE129","start0":0,"end0":85}'
        );
        $this->jsonVideoData = new jsonVideoData($inputarr);
        $this->jsonVideoData->validate();

        $this->psm = new playedSegmentMap($this->jsonVideoData, "0x0000000000000000", "0x0000000000000000");

        $this->assertEquals("0x0007ffffffffffff", $this->psm->getTop50String());
        $this->assertEquals("0x0001ffffffffffff", $this->psm->getBottom50String());
    }
}
?>