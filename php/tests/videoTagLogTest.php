<?php
declare(strict_types = 1);
require_once 'src/autoload.php';

use PHPUnit\Framework\TestCase;

// Testcases currently not passing in --process-isolation for code coverage output
// See also https://stackoverflow.com/questions/39783811/test-output-to-php-stdout-with-phpunit
// for possible fix.
//
final class videoTagLogTest extends TestCase
{
    protected $vtl;
    
    public function testConstructor()
    {
        $this->vtl = new videoTagLog();
        $this->assertNotEquals(null, $this->vtl);
    }
    
    function testaddRecord()
    {
        // Manually setup _POST['x'] data
        $this->vtl = new videoTagLog();
        $_SERVER['HTTP_REFERER'] = "http://localhost:8888/VVS/html/videotest3.html";
        $inputarr=array(
            "x" => '{"contact_id":"2","duration":84.822494,"timerange_length":2,"userid":"OPEXTRAS_5b5e0c01e5554",
                    "start0":0,"end0":2.483251,"start1":30,"end1":40.483251}'
        );
        $js = new jsonVideoData($inputarr);
        $js->validate();
        $this->assertEquals(true, $this->vtl->addRecord($js, 1));
   }
   
   public function testlistVideos()
   {
       $this->vtl = new videoTagLog();
       $this->assertEquals(2, $this->vtl->listVideos("bradford.dobson@gmail.com","OPE"));
       $this->assertEquals(0, $this->vtl->listVideos("xbrad@bdobson.net","OPE"));
   }
   
   public function testretrieveEntries()
   {
       if (false) {
       $_GET['pname']="http://localhost:8888/VVS/html/videotest3.html";
       $_GET['userid']="OPEXTRAS_5b5e0c01e5554";
       $this->vtl = new videoTagLog();
       $this->assertEquals(2, $this->vtl->retrieveEntries());
       $_GET['pname']="http://localhost:8888/VVS/html/xvideotest3.html";
       $_GET['userid']="xOPEXTRAS_5b5e0c01e5554";
       $this->assertEquals(0, $this->vtl->retrieveEntries());
       } else {
           $this->assertEquals(1,1);
       }
   }
   
   public function testShowStats()
   {
       $_GET['pname']="http://localhost:8888/VVS/html/videotest3.html";
       $_GET['userid']="OPEXTRAS_5b5e0c01e5554";
       $this->vtl = new videoTagLog();
       $this->vtl->retrieveEntries();
       $this->assertEquals(true, $this->vtl->showStats());
       $this->expectOutputRegex("/\[1\, 0\]/");
   }
   
   public function testshutdown()
   {
       $this->vtl = new videoTagLog();
       $this->assertEquals(true, $this->vtl->shutdown());
   }
}
?>