<?php
declare(strict_types = 1);

require_once 'src/vvsPercentBits.php';

use PHPUnit\Framework\TestCase;

/**
 * vvsSiteVariables test case.
 */
final class vvsPercentBitsTest extends TestCase
{

    protected $vvsPercentBits;

    public function testCanConstructOK()
    {
        $this->vvsPercentBits = new vvsPercentBits(0x00040f0000ff000f, 0x0000000b0000000a);

        $this->assertEquals("0x00040f0000ff000f", $this->vvsPercentBits->stringify(true));
        $this->assertEquals("0x0000000b0000000a", $this->vvsPercentBits->stringify(false));
    }

    public function testSetBitLow1()
    {
        $this->vvsPercentBits = new vvsPercentBits(0x0, 0x0);
        $this->vvsPercentBits->setBitByPercentage(1, 1);
        $this->assertEquals("0x0000000000000001", $this->vvsPercentBits->stringify(false));
        $this->vvsPercentBits->setBitByPercentage(1, 0);
        $this->assertEquals("0x0000000000000000", $this->vvsPercentBits->stringify(false));
    }

    public function testSetBitLow49()
    {
        $this->vvsPercentBits = new vvsPercentBits(0x0, 0x0000000b0000000a);
        $this->vvsPercentBits->setBitByPercentage(49, 1);
        $this->assertEquals("0x0001000b0000000a", $this->vvsPercentBits->stringify(false));
        $this->vvsPercentBits->setBitByPercentage(49, 0);
        $this->assertEquals("0x0000000b0000000a", $this->vvsPercentBits->stringify(false));
    }

    public function testSetHi1()
    {
        $this->vvsPercentBits = new vvsPercentBits(0x0, 0x0);
        $this->vvsPercentBits->setBitByPercentage(50, 1);
        $this->assertEquals("0x0000000000000001", $this->vvsPercentBits->stringify(true));
        $this->vvsPercentBits->setBitByPercentage(50, 0);
        $this->assertEquals("0x0000000000000000", $this->vvsPercentBits->stringify(true));
    }

    public function testSetBitHi100()
    {
        $this->vvsPercentBits = new vvsPercentBits(0x00000f0000ff000f, 0x0);
        $this->vvsPercentBits->setBitByPercentage(100, 1);
        $this->assertEquals("0x00040f0000ff000f", $this->vvsPercentBits->stringify(true));
        $this->vvsPercentBits->setBitByPercentage(100, 0);
        $this->assertEquals("0x00000f0000ff000f", $this->vvsPercentBits->stringify(true));
    }

    public function testGetBit()
    {
        $this->vvsPercentBits = new vvsPercentBits(0x00000f0000ff000f, 0x0);
        $this->vvsPercentBits->setBitByPercentage(100, 1);
        $this->assertTrue($this->vvsPercentBits->getbit(100));
        $this->vvsPercentBits->setBitByPercentage(1, 1);
        $this->assertTrue($this->vvsPercentBits->getbit(1));
    }
}
?>